package models;

import javax.persistence.Entity;
import javax.persistence.Lob;

import play.data.validation.MaxSize;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Programa extends Model {
	
	public String nombre;
	
	@MaxSize(30000)
	public String objetivo;
	
	
	@MaxSize(30000)
	public String perfilOcupacional;
	

	@MaxSize(30000)
	public String tituloOtorga;
	
	
	@MaxSize(30000)
	public String formaPago;
	
	
	@MaxSize(30000)	
	public String lineasInvestigacion;
	
	@MaxSize(30000)
	public String horario;
	
	public boolean activo;
	
	@MaxSize(30000)
	public String estado;
	
	public Blob malla;
	
	public Programa(String nombre, String objetivo, String perfilOcupacional,
			String tituloOtorga, String formaPago, String lineasInvestigacion,
			String horario, boolean activo, String estado) {
		super();
		this.nombre = nombre;
		this.objetivo = objetivo;
		this.perfilOcupacional = perfilOcupacional;
		this.tituloOtorga = tituloOtorga;
		this.formaPago = formaPago;
		this.lineasInvestigacion = lineasInvestigacion;
		this.horario = horario;
		this.activo = activo;
		this.estado = estado;
	}
	
	@Override
	public void _delete(){
		super._delete();		
		malla.getFile().delete();
	}
	
    
}
