package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.data.validation.MaxSize;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Noticia extends Model {
	
	
	@MaxSize(20000)
	public String titulo;
		
	@MaxSize(20000)
	public String contenido;
	public Blob imagen;	
	public Date fecha;
	
	@ManyToOne
	public Usuario autor;
	
	@Override
	public void _delete(){
		super._delete();
		imagen.getFile().delete();
	}
    
}
