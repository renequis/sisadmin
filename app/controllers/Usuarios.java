package controllers;



import java.util.List;

import models.Rol;
import models.Usuario;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Usuarios extends Controller {

	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user);
		       
		    }
			}catch(Exception ex){
				
			}
	}

    
    //Metodo que se invoca a traves del link que se envia al correo del usuario
    public static void activarCuenta(String identificador){
    	Usuario user=Usuario.find("byIdentificador",identificador).first();
    	user.activo=true;
    	user.save();	 
    	mensajeActivacion();
    }
    
    
    //Para generar un identificador que permita crear un link de activacion de la cuenta de usuario
    public static String generarIdentificador(String email,String apellido,String nombre){
		String identificador=email;		
		return identificador;
	}
    
    //Generando el link de activacion que se envia al correo del usuario 
    public static void enviarConfirmacion(Usuario usuario){
    	String url="http://localhost:9000/usuarios/activarCuenta?identificador="+usuario.identificador;
    	String email=usuario.email;
    	Mails.confirmarRegistro(email, url);
    	if(usuario.rol.descripcion.equals("postulante")){
    	pedirActivacion();
    	}else{
    		flash.put("confirmacionGuardado",usuario.email);
    		administracionPersonal();
    	}
    	
    }
    
    
    //Mensaje de informacion al usuario pidiendole que active su cuenta
    public static void pedirActivacion(){
    	render();    	
    }
    
    //Mensaje de informacion diciendole al usuario que su cuenta ha sido activada
    public static void mensajeActivacion(){
    	render();
    }
    
   
    
    @Check("administrador")
    //Proceso para guardar un Nuevo Usuario que sea persoal de posgrados Validando el email y password
    public static void guardarPersonal(String nombre, String apellido, String email,Long idRol) {
    	

			Rol rol=Rol.findById(idRol);
		


			
			//buscando si el usuario ya ha sido registrado anteriormente
			Usuario user = Usuario.find("byEmail",email).first(); 
			
			if (user == null){
				String identificador=generarIdentificador(email, apellido, nombre);
			
				String password=email;
				
				Usuario u=new Usuario(nombre,apellido,email,password,identificador,false,rol);				
				u.save();
				flash.put("confirmacionGuardado",true);
				enviarConfirmacion(u);			
			}else{
		
			flash.put("existente", "Error: El email ya esta en uso.");
			administracionPersonal();
			}
	
			   	
    }  
    
    
    
    
    //Metodo que muestra el listado de usuarios para administrar
    @Check("administrador")
    public static void administracionPersonal(){            	
    	render();
    }
    
    
    //Proceso para realizar cambios en la informacion del personal de posgrados relacionado con el proceso de admision
    @Check("administrador")
    public static void actualizarPersonal(Long idUsuario, String nombre, String apellido, String email, Long idRol, Boolean estado ){
    
    	Usuario usuario=Usuario.findById(idUsuario);
    	usuario.nombre=nombre;
    	usuario.apellido=apellido;
    	usuario.email=email;
     	Rol rol=Rol.findById(idRol);
    	usuario.rol=rol;
    	
    	if(estado!=null){
    		usuario.activo=true;
    	}else{
    		usuario.activo=false;
    	}
    	usuario.save();
    	flash.put("confirmacionActualizacion", true);
    	
    	administracionPersonal();
    }
    
    @Check("administrador")
    public static void resetearPasswordPersonal(Long id){
    	Usuario usuario=Usuario.findById(id);
    	usuario.password=usuario.email;
    	usuario.save();
    	
    	administracionPersonal();
    	
    }
    
    
 
    
    public static void buscarPersonal(String cadenaBusqueda){    
    	List<Rol>		roles=Rol.find("descripcion<>'administrador' and descripcion<>'postulante'").fetch();
  	  	List<Usuario> usuarios=Usuario.find("rol.descripcion<>'postulante' and lower(trim(nombre)) || lower(trim(apellido)) || lower(email) like ? order by id desc",  "%"+cadenaBusqueda.toLowerCase()+"%").fetch();	  
  	  	render(usuarios,roles);	  
    }
    
    
    //Para evitar repetir un email en una actualizacion
    public static boolean verificarEmail(String email,Long idUsuario){
    	
    	Usuario usuario=Usuario.findById(idUsuario);
    	
    	Usuario usuarioEncontrado=Usuario.find("byEmail",email).first();
    	
    	
    	return usuarioEncontrado==null || usuario==usuarioEncontrado ;
    	
    }
    
  //Para evitar repetir un email cuando se registra un nuevo Usuario
    public static boolean verificarEmailNuevo(String email){
    	    	
    	
    	Usuario usuarioEncontrado=Usuario.find("byEmail",email).first();
    	
    	
    	return usuarioEncontrado==null ;
    	
    }
    
    
    //para eliminar personal de posgrados
	    public static void eliminarPersonal(Long id){
	    	Usuario u=Usuario.findById(id);
	    	u.delete();
	    	flash.put("confirmacionEliminacion", true);
	    	administracionPersonal();
  
    }
       
}
