package controllers;

import models.Usuario;
import play.mvc.Before;
import play.mvc.Controller;

public class Postulaciones extends Controller {
	
	
	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user); 
		    }
			}catch(Exception ex){
				
			}
	}
	
    public static void index() {
        render();
    }
    
    public static void datosPersonales() {
        render();
    }
    
    public static void datosProfesionales(){
    	render();
    }
    
    
    public static void datosLaborales(){
    	render();
    }
    

    
    
    public static void documentacion(){
    	render();
    }


    
}
