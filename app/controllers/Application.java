package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import com.sun.org.apache.xpath.internal.operations.Equals;

import models.*;

public class Application extends Controller {

	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user); 
		    }
			}catch(Exception ex){
				
			}
	}
    public static void index() {    		    		
    		render();    	        
    }
    
    //Metodo para poder visualizar las imagenes sin necesidad de iniciar sesion
    public static void verImagenNoticias(Long id){    	
    	Noticia noticia=Noticia.findById(id);
    	notFoundIfNull(noticia);
    	response.setContentTypeIfNotSet(noticia.imagen.type());
    	renderBinary(noticia.imagen.get());
    }
    
    public static void noticia(Long idNoticia){
    	
    	Noticia noticia=Noticia.findById(idNoticia);
    	render(noticia);
    	
    }
   
    public static void programa(Long id){
    	
    	Programa programa=Programa.findById(id);
    	render(programa);
    	
    }

}