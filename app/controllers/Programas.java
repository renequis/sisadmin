package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import models.Programa;
import models.Usuario;
import play.Play;
import play.db.jpa.Blob;
import play.libs.MimeTypes;
import play.mvc.Before;
import play.mvc.Controller;

public class Programas extends Controller {
	
	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user);
		       
		    }
			}catch(Exception ex){
				
			}
	}
	
    public static void index() {
        render();
    }
    
    
    public static void guardar(String nombre, String objetivo, String perfilOcupacional,
    			String tituloOtorga, String formaPago, String lineasInvestigacion,
    			String horario, boolean activo, String estado, File malla) throws FileNotFoundException{
    	    	    
    	
    	Programa nuevoPrograma=new Programa(nombre, objetivo, perfilOcupacional, tituloOtorga, formaPago, lineasInvestigacion, horario, activo, estado);    	

    	//Capturando el archivo con la malla    		
	    nuevoPrograma.malla=new Blob();
	    nuevoPrograma.malla.set(new FileInputStream(malla),MimeTypes.getContentType(malla.getName()));    		    	    	
    	nuevoPrograma.save();    
    	System.out.println("Tratando de guardar");
    	index();
    	
    	//String pathMallasProgramas=Play.application().configuration().getString("myUploadPath");
    	
    	

    }

}
