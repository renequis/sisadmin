package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;

import models.Noticia;
import models.Usuario;
import play.db.jpa.Blob;
import play.libs.MimeTypes;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Noticias extends Controller {

	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user);
		       
		    }
			}catch(Exception ex){
				
			}
	}
	
    public static void index() {
        render();
    }
    
    public static void guardar(File imagen, String titulo, String contenido) throws FileNotFoundException{
    	
    	Noticia nuevaNoticia=new Noticia();    	
    	nuevaNoticia.imagen=new Blob();
    	nuevaNoticia.imagen.set(new FileInputStream(imagen), MimeTypes.getContentType(imagen.getName()));
    	
    	nuevaNoticia.titulo=titulo;
    	nuevaNoticia.contenido=contenido;
    	
    	Date fechaActual=new Date();
    	
    	nuevaNoticia.autor=Usuario.find("byEmail", Security.connected()).first();
    	nuevaNoticia.fecha=fechaActual;
 
    	
    	nuevaNoticia.save();
    	
    	index();
    	
    }
    
    public static void eliminar(Long id){
    	
    	Noticia noticia=Noticia.findById(id);
    	noticia.delete();
    	
    	
    }
    
  

}
