package controllers;

import models.Rol;
import models.Usuario;
import play.mvc.*;

public class Security extends Secure.Security {

	@Before
	public static void mostrarUsuario(){
		try{
			Usuario user = Usuario.find("byEmail", Security.connected()).first();
			//obteniendo datos del usuario que ha iniciado sesion
			if(Security.isConnected()) {
		        renderArgs.put("conectado", user);
		       
		    }
			}catch(Exception ex){
				
			}
	}
	

    //Para autenticar Usuarios -> Falta cerificacion del estado de la cuenta activo o no
	public static boolean authenticate(String username, String password) {
		 Usuario user = Usuario.find("byEmail", username).first();
	      if( user != null){ 	    	  
	    		  if( user.activo){
	    			  
	    			  if( user.password.equals(password)){	
		    		  	return true;
	    			  }else{
	    				  flash.put("clave","ERROR: Contraseña Incorrecta.");
	    				  return false;
	    			  }
	    			  
		    	  }else{
		    		  flash.put("inactivo","ERROR: Esta cuenta aún no ha sido Activada. Revise su correo electrónico.");
		    		  return false;
		    	  }
	      }else{	
	    	  flash.put("usuario","ERROR: Este email no esta registrado");
	    	  return false;
	      }
	}
	
	
	
	//Metodo para identificar el perfil del usuario conectado
	 public static boolean check(String profile) {
	       Usuario user = Usuario.find("byEmail", connected()).first();
	       
	       if(user == null){
	    	   errorPermisos();
	           return false;
	       }
	       else {

	           if (user.rol.descripcion.equals(profile)) {
		           return true;
		       }else{
	           errorPermisos();
	           return false;
		       }
	       }
	   }  
	 
	 	public static void errorPermisos(){
	 		render();
	 	}
	 
	 
	 
	  //Vista para Registrar un Nuevo Usuario
	    public static void registro() {    	
	        render();
	    }
	    
	    
	  //Proceso para guardar un Nuevo Usuario Validando el email y password
	    public static void guardarPostulante(String nombre, String apellido, String email,String password, String confirm_password) {
	    	
	    	flash.put("nombre", nombre);
			flash.put("apellido", apellido);
			flash.put("email", email);
			
			//verificando si los password proporcionados por el usuario coinciden
			if(password.equals(confirm_password)){
				
				//buscando si el usuario ya ha sido registrado anteriormente
				Usuario user = Usuario.find("byEmail",email).first(); 
				
				if (user == null){
					String identificador=Usuarios.generarIdentificador(email, apellido, nombre);
					Rol rol=Rol.findById(1L);//Asignando un rol al Usuario por defecto es POSTULANTE
					Usuario u=new Usuario(nombre,apellido,email,password,identificador,false,rol);
					u.save();	
					Usuarios.enviarConfirmacion(u);			
				}else{
			
				flash.put("existente", "Error: El email: <b>"+email+"</b>, ya esta en uso.");
				registro();
				}
		
			}else
			{					
				flash.error("Error: Las <b> contraseñas </b> ingresadas no coinciden.");
				registro();
			}  	   	
	    }
	    
	  //Para evitar repetir un email cuando se registra un nuevo Usuario
	    public static boolean verificarEmailNuevo(String email){
	    	    	
	    	
	    	Usuario usuarioEncontrado=Usuario.find("byEmail",email).first();
	    	
	    	
	    	return usuarioEncontrado==null ;
	    	
	    }
    

}
